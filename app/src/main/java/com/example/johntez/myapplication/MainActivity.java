package com.example.johntez.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    //declaring button object
    private Button convert, exit;
    private EditText millitext,inchestext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //register buttons

       convert = (Button) findViewById(R.id.convert_button);
       exit = (Button) findViewById(R.id.exit_button);

        //listeners

        convert.setOnClickListener(this);
        exit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        //events

        if (v==convert){

            //registering edit views

            millitext = (EditText) findViewById(R.id.input_millimeter);
            inchestext = (EditText) findViewById(R.id.input_inches);

            double milli;
            double inches ;

            if(millitext.getText() != null) {

               milli = Double.parseDouble(millitext.getText().toString());
                inches =(milli/25.4);
                inchestext.setText(Double.toString(inches));

            }


        }else if (v==exit){

            finish();

        }

    }
}
